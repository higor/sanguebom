-- sanguebom
CREATE DATABASE sanguebom;

SET search_path TO sanguebom;



-- Campanha
CREATE TABLE public.Campanha(
  id_campanha SERIAL PRIMARY KEY,
  nome VARCHAR(100) NOT NULL,
  tipo_sanguineo CHAR(2) NOT NULL,
  local_doacao VARCHAR(100) NOT NULL,
  endereco VARCHAR(200),
  telefone CHAR(10)
);




-- Usu�rio
CREATE TABLE public.Usuario (
	id_usuario SERIAL PRIMARY KEY,
	nome VARCHAR(80) NOT NULL,
	login VARCHAR(30) NOT NULL UNIQUE,
	senha CHAR(32) NOT NULL,
	email VARCHAR(50)	
);

-- Tabela Papel
CREATE TABLE public.Papel (
	id_papel SERIAL PRIMARY KEY,
	nome VARCHAR(80) NOT NULL
);

-- Tabela Recurso
CREATE TABLE public.Recurso (
	id_recurso SERIAL PRIMARY KEY,
	nome VARCHAR(80) NOT NULL,
	chave INT NOT NULL UNIQUE,
	titulo VARCHAR(80) NOT NULL,
	link VARCHAR(80) NOT NULL,
	publico BOOLEAN NOT NULL
);

-- Tebela Usuario_Papel
CREATE TABLE public.Usuario_Papel (
	id_usuario_papel SERIAL PRIMARY KEY,
	id_usuario INT NOT NULL 
		REFERENCES public.Usuario,
	id_papel INT NOT NULL
		REFERENCES public.Papel
);

-- Tebela Papel_Recurso
CREATE TABLE Papel_Recurso (
	id_papel_recurso SERIAL PRIMARY KEY,
	id_papel INT NOT NULL REFERENCES public.Papel,
	id_recurso INT NOT NULL
		REFERENCES public.Recurso
);

