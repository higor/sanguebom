<%@include file="/WEB-INF/jsp/topo.jsp" %>


<div class="well">

	<h4>Lista de campanhas</h4>
	<table class="table table-striped">
	<tr>
		<td>ID</td>
		<td>Nome</td>
		<td>Telefone</td>
		<td>Endere�o</td>
		<td>Local doa��o</td>
		<td>A��es</td>		
	</tr>
	<c:forEach items="${campanhas}" var="campanha">
        <tr>
        	<td>${campanha.idCampanha}</td>
        	<td>${campanha.nome}</td>
            <td>${campanha.telefone}</td>
            <td>${campanha.endereco}</td>
            <td>${campanha.localDoacao}</td>
            <td>
            	<a href="/sanguebom/campanha?id=${campanha.idCampanha}" class="btn">Ver campanha</a>
            	<c:if test="${sessionScope.usuario != null }">
            		<a href="/sanguebom/campanha/deletar?id=${campanha.idCampanha}" class="btn">Deletar</a>
            		<a href="/sanguebom/campanha/atualizar?id=${campanha.idCampanha}" class="btn">Atualizar</a>
            	</c:if>
            </td>
        </tr>
    </c:forEach>
	</table>
	
</div>

<%@include file="/WEB-INF/jsp/rodape.jsp" %>