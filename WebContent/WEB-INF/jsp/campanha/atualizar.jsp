<%@include file="/WEB-INF/jsp/topo.jsp" %>


<div class="well">

	<h4>Atualizar Campanha</h4>
	
	<form method="post">
		
		<label for="nome">Nome:</label>
		<input type="text" name="nome" value="${campanha.nome}" />
		
		<label for="nome">Tipo Sangu�neo:</label>
		<input type="text" name="tipoSanguineo" value="${campanha.tipoSanguineo}"/>
	
		<label for="nome">Local:</label>
		<input type="text" name="localDoacao" value="${campanha.localDoacao}"/>
		
		<label for="nome">Endere�o:</label>
		<input type="text" name="endereco" value="${campanha.endereco}" />
	
		<label for="nome">Telefone:</label>
		<input type="text" name="telefone" value="${campanha.telefone}"/>
		
		<label for="nome">Data in�cio:</label>
		<input type="date" name="data_inicio" value="${campanha.dataInicio}"/>
		
		<label for="nome">Data fim:</label>
		<input type="date" name="data_fim" value="${campanha.dataFim}"/>
		
		<input type="hidden" name="idCampanha" value="${campanha.idCampanha}"/>
		<br/>
		<input type="submit" value="Atualizar" class="btn btn-primary" />
	
	</form>
	
</div>

<%@include file="/WEB-INF/jsp/rodape.jsp" %>