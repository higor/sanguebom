<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="pt-br">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SangueBom</title>


<link type="text/css" rel="stylesheet"
		href="/sanguebom/css/bootstrap.min.css">

<script type="text/javascript"
	src="/sanguebom/js/bootstrap.min.js">
</script>


</head>
<body class="container">

	<div class="well">
		<h2>SangueBom</h2>
	</div>
	
	<%-- Menu  --%>
	<%@include file="/WEB-INF/jsp/menu.jsp" %>
	
	
	<%-- Mensagens --%>
	<c:forEach items="${mensagens}" var="mensagem">
		<div class="alert alert-block">
			${mensagem}
		</div>
	</c:forEach>
	
	
	
	
	
	
	

