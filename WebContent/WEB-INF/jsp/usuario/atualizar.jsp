<%@include file="/WEB-INF/jsp/topo.jsp" %>


<div class="well">

	<h4>Cadastro de usu�rio</h4>
	
	<form method="post">
		
		<label for="nome">Nome <strong>*</strong>:</label>
		<input type="text" name="nome" value="${usuario.nome}"/>
		
		<label for="nome">Email <strong>*</strong>:</label>
		<input type="text" name="email" value="${usuario.email}"/>
	
		<label for="nome">Senha <strong>(Caso n�o queira mudar a senha, deixar campo em branco)</strong>:</label>
		<input type="password" name="senha" value="" />
		
		<hr/>
		
		<input type="submit" value="Cadastrar"  class="btn btn-primary"/>
	
	</form>
	
</div>

<%@include file="/WEB-INF/jsp/rodape.jsp" %>