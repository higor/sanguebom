<div class="navbar">
  <div class="navbar-inner">

    <ul class="nav">
      <li>
      	<a href="/sanguebom">
      		In�cio
    	</a>
      </li>
      <c:if test="${sessionScope.usuario != null }">
      <li>
      	<a href="/sanguebom/campanha/cadastro">
      		Cadastrar Campanha
      	</a>
      </li>
      </c:if>
      <li>
      	<a href="/sanguebom/campanha/lista">
      		Listar Campanhas
      	</a>
      </li>
    </ul>
    
    <ul class="nav pull-right">
    
   		<c:choose>
   		
	   		<c:when test="${sessionScope.usuario != null}">
		    	<li>
	    			<a href="/sanguebom/usuario/atualizar">${sessionScope.usuario.login}</a>
	    		</li>
	    		<li>
	    			<a href="/sanguebom/seguranca/sair">
	    				Sair
	    			</a>
	    		</li>
	   		</c:when>
   			<c:otherwise>
   				<li>
		    		<a href="/sanguebom/usuario/cadastro">
		    			Cadastrar-se
		    		</a>
		    	</li>
 		    	<li>
		    		<a href="/sanguebom/seguranca/autenticar">
		    			Login
		    		</a>
		    	</li>
    		</c:otherwise>
   		
   		
   		
   		</c:choose>

    
    </ul>
    
  </div>
</div>