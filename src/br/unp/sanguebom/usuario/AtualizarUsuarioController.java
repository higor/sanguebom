package br.unp.sanguebom.usuario;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.unp.sanguebom.app.AppController;
import br.unp.sanguebom.campanha.Campanha;
import br.unp.sanguebom.dao.CampanhaDAO;
import br.unp.sanguebom.dao.UsuarioDAO;
import br.unp.sanguebom.seguranca.SegurancaHelper;
import br.unp.sanguebom.seguranca.Usuario;
@WebServlet("/usuario/atualizar")
public class AtualizarUsuarioController extends AppController {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Usuario u = (Usuario)
		request.getSession()
			.getAttribute("usuario");

		new SegurancaHelper()
			.autorizar(u, 1);
		try {
			UsuarioDAO dao = new UsuarioDAO();
			
			Usuario user = dao.getUsuario(u);
			
			request.setAttribute("usuario", user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			addMensagem("Comportamento inesperado...");
		}
		
		forward("/usuario/atualizar.jsp",
				request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Usuario u = (Usuario)
		request.getSession()
			.getAttribute("usuario");

		new SegurancaHelper()
			.autorizar(u, 1);
		if(!u.getLogin().equals("")) {
			// Recuperar dados do formulário
			Usuario usuario = new Usuario();
			
			usuario.setNome(request.getParameter("nome"));
			usuario.setEmail(request.getParameter("email"));
			if(!request.getParameter("senha").equals("")){
				UsuarioDAO dao = new UsuarioDAO();
				try {
					Usuario user = dao.getUsuario(u);
					
					usuario.setSenha(request.getParameter("senha"));
					usuario.setLogin(user.getLogin());
					dao.atualizarSenha(usuario);
				} catch (Exception e) {
					e.printStackTrace();
					addMensagem("Comportamento inesperado...");
				}
			}
			// Persistir no SGBD
			try {
				
				new UsuarioDAO().atualizar(usuario);
				request.setAttribute("usuario", usuario);
				addMensagem("Usuário atualizada com sucesso!");
				
			} catch (Exception e) {
				e.printStackTrace();
				addMensagem("Comportamento inesperado...");
			}
		}
		forward("/usuario/atualizar.jsp",
				request, response);
	}
}
