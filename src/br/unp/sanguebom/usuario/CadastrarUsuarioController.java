package br.unp.sanguebom.usuario;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.unp.sanguebom.app.AppController;
import br.unp.sanguebom.campanha.Campanha;
import br.unp.sanguebom.dao.CampanhaDAO;
import br.unp.sanguebom.dao.UsuarioDAO;
import br.unp.sanguebom.seguranca.SegurancaHelper;
import br.unp.sanguebom.seguranca.Usuario;

@WebServlet("/usuario/cadastro")
public class CadastrarUsuarioController  extends AppController {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * HTTP GET
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		forward("/usuario/cadastro.jsp",
					request, response);
	}

	/**
	 * HTTP POST
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String retorno = "/usuario/cadastro.jsp";
		
		// Recuperar dados do formul�rio
		Usuario usuario = new Usuario();
		
		usuario.setNome(request.getParameter("nome"));
		usuario.setEmail(request.getParameter("email"));
		usuario.setSenha(request.getParameter("senha"));
		usuario.setLogin(request.getParameter("login"));
		// Persistir no SGBD
		try {
			if( request.getParameter("nome").equals("") || request.getParameter("email").equals("") ||
			request.getParameter("senha").equals("") || request.getParameter("login").equals("")) {
				addMensagem("Todos os campos com * s�o obrigat�rios!");
			} else {
				new UsuarioDAO().cadastrar(usuario);
				addMensagem("Cadatro realizado com sucesso!");
				retorno = "/sanguebom/seguranca/autenticar";
			}
			
			
		} catch (java.sql.SQLException e) {
			if(e.getMessage().contains("violates unique")) {
				addMensagem("Login j� cadastrado, por favor, escolha outro");
			}
		} catch (Exception e) {
			e.printStackTrace();
			addMensagem("Comportamento inesperado...");			
		}
		
		// Exibir confirma��o para o usu�rio
		forward(retorno,
				request, response);		
		
	}
}
