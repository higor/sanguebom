package br.unp.sanguebom.seguranca;

import java.util.ArrayList;


/**
 * Um usu�rio re�ne as informa��es as informa��es
 * necess�rias para identificar os atores que
 * interagem com o sistema.
 * 
 * @author Weinberg
 */
public class Usuario {

	/**
	 * Chave prim�ria do usu�rio.
	 */
	private int idUsuario;
	
	/**
	 * Nome do usu�rio.
	 */
	private String nome;
	
	/**
	 * Login do usu�rio. Usado para identifica��o em 
	 * conjunto com a senha.
	 */
	private String login;
	
	/**
	 * Senha do usu�rio. Usada para identifica��o em 
	 * conjunto com o login.
	 */
	private String senha;
	
	/**
	 * Email do usu�rio. Usado em situa��es de contato.
	 */
	private String email;
	
	
	/**
	 * Papeis que o usu�rio possui.
	 */
	private ArrayList<Papel> papeis;

	
	/**
	 * Construtor padr�o.
	 */
	public Usuario() {
		
	}
	
	
	
	
	/**
	 * M�todos acessores.
	 */
	
	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public ArrayList<Papel> getPapeis() {
		return papeis;
	}

	public void setPapeis(ArrayList<Papel> papeis) {
		this.papeis = papeis;
	}
	
	
	
}
