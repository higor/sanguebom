package br.unp.sanguebom.seguranca;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import br.unp.sanguebom.dao.SegurancaDAO;

import com.sun.servicetag.UnauthorizedAccessException;

public class SegurancaHelper {

	/**
	 * Realiza a criptografia MD5.
	 * 
	 * @param palavra
	 * @return
	 */
	public String MD5(String palavra) {
		
		String MD5 = null;
		
		try {
			MessageDigest md = 
				MessageDigest.getInstance("MD5");
			
			BigInteger hash = 
			new BigInteger(1, 
					md.digest(palavra.getBytes())
					);
			
			MD5 = hash.toString(16);
			

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} 
		
		
		return MD5;
	}
	
	/**
	 * M�todo utilizado para autenticar o usu�rio.
	 * 
	 * @param login
	 * @param senha
	 * @return
	 */
	public boolean autenticar(String login, 
								String senha) {
		
		boolean autenticou = false;
		
		// Criptografando senha.
		senha = this.MD5(senha);
		
		// Buscando informa��es no SGBD.
		autenticou = 
			new SegurancaDAO().autenticar(login,
											senha);
		
		
		return autenticou;
	}
	
	/**
	 * M�todo utilizado para autorizar o usu�rio.
	 * 
	 * @param login
	 * @param chave_recurso
	 * @return
	 */
	public boolean autorizar(Usuario usuario, 
							int chave_recurso)
		 {
		
		boolean autorizado = false;
		
		if(usuario != null) {
			autorizado = new SegurancaDAO()
					.autorizar(usuario.getLogin(), 
								chave_recurso);
		}
		
		if (!autorizado) {
			throw new UnauthorizedAccessException();
		}
					
		return autorizado;
		
	}
	
	
}
