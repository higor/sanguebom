package br.unp.sanguebom.seguranca;

/**
 * Enum contendo todos os recursos disponíveis na aplicação.
 * 
 * @author Weinberg
 */
public enum RecursosDisponiveis {

	// Recursos disponíveis
	CADASTRAR_NOTICIA(1),
	LISTAR_NOTICIAS(2),
	ALTERAR_NOTICIA(3),
	EXCLUIR_NOTICIA(4);
	
	
	
	/**
	 * Chave do recurso para identificação hard coded.
	 */
	private int chave;
	
	
	
	/**
	 * Construtor.
	 * @param chave
	 */
	private RecursosDisponiveis(int chave) {
		this.chave = chave;
	}

	
	
	/**
	 * Métodos acessores.
	 */
	
	public int getChave() {
		return chave;
	}

	public void setChave(int chave) {
		this.chave = chave;
	}
	
				
}
