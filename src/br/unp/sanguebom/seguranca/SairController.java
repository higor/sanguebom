package br.unp.sanguebom.seguranca;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.unp.sanguebom.app.AppController;

/**
 * Servlet implementation class SairController
 */
@WebServlet("/seguranca/sair")
public class SairController extends AppController {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Invalidando a sess�o
		request.getSession().invalidate();
		
		forward("index.jsp", request, response);
		
	}


}
