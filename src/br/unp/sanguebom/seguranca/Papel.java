package br.unp.sanguebom.seguranca;

import java.util.ArrayList;


/**
 * Um papel define um conjunto de recursos que
 * um determinado usu�rio possui acesso.
 * Por exemplo: Cadastro de Campanha, etc.
 * 
 * @author Weinberg
 */
public class Papel {

	/**
	 * Chave prim�ria do papel.
	 */
	private int idPapel;
	
	/**
	 * Nome do papel.
	 * Usado para identificar as suas caracter�sticas principais.
	 */
	private String nome;

	/**
	 * Recursos que o papel possui.
	 */
	private ArrayList<Recurso> recursos;
	
	
	
	/**
	 * Construtor padr�o.
	 */
	public Papel() {
		
	}
	
	
	
	/**
	 * M�todos acessores.
	 */
	
	public int getIdPapel() {
		return idPapel;
	}

	public void setIdPapel(int idPapel) {
		this.idPapel = idPapel;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public ArrayList<Recurso> getRecursos() {
		return recursos;
	}

	public void setRecursos(ArrayList<Recurso> recursos) {
		this.recursos = recursos;
	}
	
	
}
