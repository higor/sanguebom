package br.unp.sanguebom.seguranca;


/**
 * Um recurso � qualquer funcionalidade disponibilizada pelo sistema, 
 * que precisa de autoriza��o para uso (acesso restrito).
 * Por exemplo: Cadastro de Campanha, etc.
 * 
 * @author Weinberg
 */
public class Recurso {

	/**
	 * Chave prim�ria do recurso.
	 */
	private int idRecurso;
	
	/**
	 * Nome do recurso. Usado para identificar as suas 
	 * caracter�sticas principais.
	 */
	private String nome;

	/**
	 * Chave do recurso. Usado para identificar 
	 * unicamente em trechos hard codeds.
	 */
	private String chave;
	
	/**
	 * T�tulo do link que ir� aparecer no menu da aplica��o.
	 */
	private String titulo;

	/**
	 * Link que ir� aparecer no menu da aplica��o.
	 */
	private String link;

	/**
	 * Indica se o recurso � de acesso p�blico (sem precisar autenticar) ou n�o.
	 */
	private Boolean publico;

	
	/**
	 * Construtor padr�o.
	 */
	public Recurso() {
		
	}
	
	
	
	/**
	 * M�todos acessores.
	 */
	
	public int getIdRecurso() {
		return idRecurso;
	}

	public void setIdRecurso(int idRecurso) {
		this.idRecurso = idRecurso;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Boolean getPublico() {
		return publico;
	}

	public void setPublico(Boolean publico) {
		this.publico = publico;
	}
	
	
}
