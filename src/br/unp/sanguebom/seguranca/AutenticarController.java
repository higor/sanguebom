package br.unp.sanguebom.seguranca;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.unp.sanguebom.app.AppController;

/**
 * Servlet implementation class AutenticarController
 */
@WebServlet("/seguranca/autenticar")
public class AutenticarController extends AppController {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		forward("/seguranca/autenticar.jsp", 
				request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Dados do formul�rio
		String login = 
				request.getParameter("login");
		
		String senha = 
				request.getParameter("senha");
		
		
		// Helper
		boolean autenticou = 
			new SegurancaHelper().
				autenticar(login, senha);
		
		if(autenticou) {
			
			Usuario u = new Usuario();
			u.setLogin(login);
			request.getSession().
				setAttribute("usuario", u);
			
			forward("/index.jsp",
					request, response);
		}
		else {
			addMensagem("Usu�rio e/ou senha incorretos!");
			forward("/seguranca/autenticar.jsp", 
					request, response);
		}
		
		
		
		
	}

}
