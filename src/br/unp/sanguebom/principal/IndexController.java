package br.unp.sanguebom.principal;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.unp.sanguebom.app.AppController;

/**
 * Servlet implementation class IndexController
 */
@WebServlet("/index.jsp")
public class IndexController extends AppController {
	private static final long serialVersionUID = 1L;
       

	/**
	 * HTTP GET
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//request.getRequestDispatcher("/WEB-INF/jsp/index.jsp").forward(request, response);
		forward("index.jsp", request, response);
	}

	/**
	 * HTTP POST
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
