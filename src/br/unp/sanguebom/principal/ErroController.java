package br.unp.sanguebom.principal;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.unp.sanguebom.app.AppController;

/**
 * Servlet implementation class ErroController
 */
@WebServlet("/erro")
public class ErroController extends AppController {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Integer status = 
				(Integer) request.getAttribute
		("javax.servlet.error.status_code");
		
	
		if (status == null || status == 200) {
			addMensagem("Requisi��o OK!");
		}
		else if (status == 404) {
			addMensagem("P�gina n�o encontrada...");
		}
		else {
			String excecao = request
					.getAttribute
					("javax.servlet.error.exception").toString();
			
			if(excecao == 
				"com.sun.servicetag.UnauthorizedAccessException") {
				addMensagem("Acesso negado!");
			}
			else {
				addMensagem("Comportamento inesperado!!!");
			}
			
		}
		
		forward("/erro.jsp", request, response);
		
	}


}
