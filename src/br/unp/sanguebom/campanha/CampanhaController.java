package br.unp.sanguebom.campanha;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.unp.sanguebom.app.AppController;
import br.unp.sanguebom.dao.CampanhaDAO;

@WebServlet("/campanha" )
public class CampanhaController extends AppController {
	private static final long serialVersionUID = 1L;



	/**
	 * HTTP GET
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
		try {
			Campanha campanha = new Campanha();
			Integer id_camapanha = Integer.parseInt(request.getParameter("id"));
			campanha.setIdCampanha(id_camapanha);
			CampanhaDAO dao = new CampanhaDAO();
			
			Campanha camp = dao.getCampanhaItem(campanha);
			
			request.setAttribute("campanha", camp);
		} catch (Exception e) {
			e.printStackTrace();
			addMensagem("Comportamento inesperado...");
		}
		
		forward("/campanha/index.jsp",
					request, response);
	}
}
