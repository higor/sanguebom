package br.unp.sanguebom.campanha;

import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.unp.sanguebom.app.AppController;
import br.unp.sanguebom.dao.CampanhaDAO;
import br.unp.sanguebom.seguranca.SegurancaHelper;
import br.unp.sanguebom.seguranca.Usuario;

/**
 * Controlador respons�vel pelo cadastro
 * de Campanhas.
 * 
 * @author Weinberg
 */
@WebServlet("/campanha/cadastro")
public class CadastroCampanhaController extends AppController {
	private static final long serialVersionUID = 1L;



	/**
	 * HTTP GET
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Usuario u = (Usuario)
				request.getSession()
					.getAttribute("usuario");
		
		new SegurancaHelper()
			.autorizar(u, 1);
		

		forward("/campanha/cadastro.jsp",
					request, response);
	}

	/**
	 * HTTP POST
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		Usuario u = (Usuario)
				request.getSession()
					.getAttribute("usuario");
		
		new SegurancaHelper()
			.autorizar(u, 1);
		
		
		
		// Recuperar dados do formul�rio
		Campanha campanha = new Campanha();
		
		campanha.setNome(request.getParameter("nome"));
		campanha.setTipoSanguineo(request.getParameter("tipoSanguineo"));
		campanha.setLocalDoacao(request.getParameter("localDoacao"));
		campanha.setEndereco(request.getParameter("endereco"));
		campanha.setTelefone(request.getParameter("telefone"));
		campanha.setDataInicio(request.getParameter("data_inicio"));
		campanha.setDataFim(request.getParameter("data_fim"));
		
		// Persistir no SGBD
		try {
			if(request.getParameter("nome").equals("") || request.getParameter("tipoSanguineo").equals("") || 
					request.getParameter("localDoacao").equals("") || request.getParameter("endereco").equals("") ||
					request.getParameter("telefone").equals("") || request.getParameter("data_inicio").equals("") ||
					request.getParameter("data_fim").equals("")){
				addMensagem("Preencha todos os campos com *");
			} else {
				new CampanhaDAO().cadastrar(campanha);
				addMensagem("Cadatro realizado com sucesso!");	
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			addMensagem("Comportamento inesperado...");
		}
		
		
		// Exibir confirma��o para o usu�rio
		forward("/campanha/cadastro.jsp",
				request, response);
	}

}
