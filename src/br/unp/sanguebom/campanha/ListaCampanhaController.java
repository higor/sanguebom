package br.unp.sanguebom.campanha;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.unp.sanguebom.app.AppController;
import br.unp.sanguebom.dao.CampanhaDAO;
import br.unp.sanguebom.seguranca.SegurancaHelper;
import br.unp.sanguebom.seguranca.Usuario;

/**
 * Controlador responsável pela listagem
 * de Campanhas.
 * 
 * @author Weinberg
 */
@WebServlet("/campanha/lista")
public class ListaCampanhaController extends AppController {
	private static final long serialVersionUID = 1L;



	/**
	 * HTTP GET
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
		try {
			request.setAttribute("campanhas", 
									new CampanhaDAO().listar());
		} catch (Exception e) {
			e.printStackTrace();
			addMensagem("Comportamento inesperado...");
		}
		
		forward("/campanha/lista.jsp",
					request, response);
	}

	/**
	 * HTTP POST
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
