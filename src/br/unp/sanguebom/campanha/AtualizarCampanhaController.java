package br.unp.sanguebom.campanha;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.unp.sanguebom.app.AppController;
import br.unp.sanguebom.dao.CampanhaDAO;
import br.unp.sanguebom.seguranca.SegurancaHelper;
import br.unp.sanguebom.seguranca.Usuario;

/**
 * Servlet implementation class AtualizarCampanhaController
 */
@WebServlet({ "/AtualizarCampanhaController", "/campanha/atualizar" })
public class AtualizarCampanhaController extends AppController {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Usuario u = (Usuario)
		request.getSession()
			.getAttribute("usuario");

		new SegurancaHelper()
			.autorizar(u, 1);
		try {
			Campanha campanha = new Campanha();
			Integer id_camapanha = Integer.parseInt(request.getParameter("id"));
			campanha.setIdCampanha(id_camapanha);
			CampanhaDAO dao = new CampanhaDAO();
			
			Campanha camp = dao.getCampanha(campanha);
			
			request.setAttribute("campanha", camp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			addMensagem("Comportamento inesperado...");
		}
		
		forward("/campanha/atualizar.jsp",
				request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Usuario u = (Usuario)
		request.getSession()
			.getAttribute("usuario");

		new SegurancaHelper()
			.autorizar(u, 1);
		if(!request.getParameter("idCampanha").equals("")) {
			Campanha campanha = new Campanha();
			
			campanha.setNome(request.getParameter("nome"));
			campanha.setTipoSanguineo(request.getParameter("tipoSanguineo"));
			campanha.setLocalDoacao(request.getParameter("localDoacao"));
			campanha.setEndereco(request.getParameter("endereco"));
			campanha.setTelefone(request.getParameter("telefone"));
			campanha.setDataInicio(request.getParameter("data_inicio"));
			campanha.setDataFim(request.getParameter("data_fim"));
			Integer id_campanha = Integer.parseInt(request.getParameter("idCampanha"));
			campanha.setIdCampanha(id_campanha);

			// Persistir no SGBD
			try {
				
				new CampanhaDAO().atualizar(campanha);
				request.setAttribute("campanha", campanha);
				addMensagem("Campanha atualizada com sucesso!");
				
			} catch (Exception e) {
				e.printStackTrace();
				addMensagem("Comportamento inesperado...");
			}
		}
		forward("/campanha/atualizar.jsp",
				request, response);
	}

}
