package br.unp.sanguebom.campanha;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.unp.sanguebom.app.AppController;
import br.unp.sanguebom.dao.CampanhaDAO;
import br.unp.sanguebom.seguranca.SegurancaHelper;
import br.unp.sanguebom.seguranca.Usuario;

/**
 * Servlet implementation class DeletarCampanhaController
 */
@WebServlet("/campanha/deletar")
public class DeletarCampanhaController extends AppController {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Usuario u = (Usuario)
		request.getSession()
			.getAttribute("usuario");

		new SegurancaHelper()
			.autorizar(u, 1);

		Campanha campanha = new Campanha();
		Integer id_camapanha = Integer.parseInt(request.getParameter("id"));
		campanha.setIdCampanha(id_camapanha);
		
		CampanhaDAO dao = new CampanhaDAO();
		try {
			dao.remover(campanha);
			addMensagem("Item removido com sucesso...");
		} catch (Exception e1) {
			e1.printStackTrace();
			addMensagem("Comportamento inesperado...");			
		}
		try {
			List<Campanha> campanhas = CampanhaDAO.listar();
			request.setAttribute("campanhas", campanhas);
		} catch (Exception e) {
			e.printStackTrace();
			addMensagem("Comportamento inesperado...");
		}
		
		
		forward("/campanha/listar.jsp",
				request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
