package br.unp.sanguebom.campanha;

import java.util.Date;


public class Campanha {

	// Atributos
	private int idCampanha;
	private String nome;
	private String tipoSanguineo;
	private String localDoacao;
	private String endereco;
	private String telefone;
	private String dataInicio;
	private String dataFim;
	
	
	// Acessores
	public int getIdCampanha() {
		return idCampanha;
	}
	public void setIdCampanha(int idCampanha) {
		this.idCampanha = idCampanha;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTipoSanguineo() {
		return tipoSanguineo;
	}
	public void setTipoSanguineo(String tipoSanguineo) {
		this.tipoSanguineo = tipoSanguineo;
	}
	public String getLocalDoacao() {
		return localDoacao;
	}
	public void setLocalDoacao(String localDoacao) {
		this.localDoacao = localDoacao;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}
	public String getDataInicio() {
		return dataInicio;
	}
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}
	public String getDataFim() {
		return dataFim;
	}
	
	
}
