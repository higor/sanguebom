package br.unp.sanguebom.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import br.unp.sanguebom.app.AppDAO;
import br.unp.sanguebom.campanha.Campanha;
import br.unp.sanguebom.seguranca.SegurancaHelper;
import br.unp.sanguebom.seguranca.Usuario;

public class UsuarioDAO extends AppDAO {
	
	/**
	 * Cadastro de usu�rio no SGBD.
	 * 
	 * @throws Exception 
	 */
	public void cadastrar(Usuario usuario)
			throws Exception {
		
		// Conex�o com o SGBD.
		Connection con = getConexao();
		
		// SQL
		String sql = "INSERT INTO usuario " +
		  "(nome, login, email," +
		  "senha)" +
		"VALUES (?, ?, ?, ?)";
		
		// Preparando par�metros SQL
		PreparedStatement stmt = 
				con.prepareStatement(sql);
		SegurancaHelper senha = new SegurancaHelper();
		String senhaMD5 = senha.MD5(usuario.getSenha());
		
		stmt.setString(1, usuario.getNome());
		stmt.setString(2, usuario.getLogin());
		stmt.setString(3, usuario.getEmail());
		stmt.setString(4, senhaMD5);
		
		// Executando SQL
		stmt.execute();
		
		// Fechar conex�o com o SGBD.
		stmt.close();
		con.close();
	}
	
	/**
	 * Atualiza��o de usu�rio no SGBD.
	 * 
	 * @throws Exception 
	 */
	public void atualizar(Usuario usuario)
			throws Exception {
		
		// Conex�o com o SGBD.
		Connection con = getConexao();
		
		// SQL
		String sql = "UPDATE usuario SET " +
		  "nome = ?, login = ?, email = ?"+
		  "WHERE login = ?";
		
		// Preparando par�metros SQL
		PreparedStatement stmt = 
				con.prepareStatement(sql);
		
		stmt.setString(1, usuario.getNome());
		stmt.setString(2, usuario.getLogin());
		stmt.setString(3, usuario.getEmail());
		stmt.setString(4, usuario.getLogin());
		
		// Executando SQL
		stmt.execute();
		
		// Fechar conex�o com o SGBD.
		stmt.close();
		con.close();
	}
	
	/**
	 * Atualiza��o de usu�rio no SGBD.
	 * 
	 * @throws Exception 
	 */
	public void atualizarSenha(Usuario usuario)
			throws Exception {
		
		// Conex�o com o SGBD.
		Connection con = getConexao();
		
		// SQL
		String sql = "UPDATE usuario SET " +
		  "senha = ? "+
		  "WHERE login = ?";
		
		// Preparando par�metros SQL
		PreparedStatement stmt = 
				con.prepareStatement(sql);
		
		SegurancaHelper senha = new SegurancaHelper();
		String senhaMD5 = senha.MD5(usuario.getSenha());
		System.out.println(usuario.getSenha());
		stmt.setString(1, senhaMD5);
		stmt.setString(2, usuario.getLogin());
		
		// Executando SQL
		stmt.execute();
		
		// Fechar conex�o com o SGBD.
		stmt.close();
		con.close();
	}
	

	public Usuario getUsuario(Usuario usuario) throws Exception {
		Connection con = getConexao();
		String sql = "SELECT * " +
					"FROM usuario where login = ?";
		
		PreparedStatement stmt = con.prepareStatement(sql);
		stmt.setString(1, usuario.getLogin());
		ResultSet rs = stmt.executeQuery();
		rs.next();
		
		Usuario u = new Usuario();
		u.setIdUsuario(rs.getInt("id_usuario"));
		u.setEmail(rs.getString("email"));
		u.setLogin(rs.getString("login"));
		u.setNome(rs.getString("nome"));
		u.setSenha(rs.getString("senha"));
		stmt.close();
		return u;
	}
}
