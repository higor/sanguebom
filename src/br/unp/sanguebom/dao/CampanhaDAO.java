package br.unp.sanguebom.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import br.unp.sanguebom.app.AppDAO;
import br.unp.sanguebom.campanha.Campanha;

public class CampanhaDAO extends AppDAO {

	/**
	 * Cadastro de campanha no SGBD.
	 * 
	 * @throws Exception 
	 */
	public void cadastrar(Campanha campanha)
			throws Exception {
		
		// Conex�o com o SGBD.
		Connection con = getConexao();
		
		// SQL
		String sql = "INSERT INTO campanha " +
		  "(nome, tipo_sanguineo, local_doacao," +
		  "endereco, telefone, data_inicio, data_fim)" +
		"VALUES (?, ?, ?, ?, ?, to_date(?,'yyyy/mm/dd'), to_date(?, 'yyyy/mm/dd'))";
		
		// Preparando par�metros SQL
		PreparedStatement stmt = 
				con.prepareStatement(sql);
		
		stmt.setString(1, campanha.getNome());
		stmt.setString(2, campanha.getTipoSanguineo());
		stmt.setString(3, campanha.getLocalDoacao());
		stmt.setString(4, campanha.getEndereco());
		stmt.setString(5, campanha.getTelefone());
		stmt.setString(6, campanha.getDataInicio());
		stmt.setString(7, campanha.getDataFim());
		
		// Executando SQL
		stmt.execute();
		
		// Fechar conex�o com o SGBD.
		stmt.close();
		con.close();
		
	}
	
	
	/**
	 * Consulta de campanhas no SGBD.
	 * 
	 * @throws Exception 
	 */
	public static ArrayList<Campanha> listar()
			throws Exception {
		
		// Conex�o com o SGBD.
		Connection con = getConexao();
		
		// SQL
		String sql = "SELECT c.id_campanha, c.nome, c.tipo_sanguineo, " +
						"c.local_doacao, c.endereco, c.telefone " +
							" FROM campanha c ";
		
		// Preparando par�metros SQL
		PreparedStatement stmt = 
				con.prepareStatement(sql);
		
		// Executando SQL
		ResultSet rs = stmt.executeQuery();
		
		// Populando resultado
		ArrayList<Campanha> campanhas = new ArrayList<Campanha>();
		while (rs.next()) {
			
			Campanha c = new Campanha();

			c.setIdCampanha(rs.getInt("id_campanha"));
			c.setNome(rs.getString("nome"));
			c.setTipoSanguineo(rs.getString("tipo_sanguineo"));
			c.setLocalDoacao(rs.getString("local_doacao"));
			c.setEndereco(rs.getString("endereco"));
			c.setTelefone(rs.getString("telefone"));
			
			campanhas.add(c);
			
		};
		
		// Fechar conex�o com o SGBD.
		stmt.close();
		con.close();
		
		
		// Retornando o resultado
		return campanhas;
		
	}
	
	public Campanha getCampanha(Campanha campanha) throws Exception {
		Connection con = getConexao();
		String sql = "SELECT id_campanha, nome, tipo_sanguineo, local_doacao, endereco, telefone," + 
		"to_char(data_inicio, 'yyyy-mm-dd') as data_inicio, to_char(data_fim, 'yyyy-mm-dd') as data_fim " +
		"FROM campanha where id_campanha = ?";
		
		PreparedStatement stmt = con.prepareStatement(sql);
		stmt.setLong(1, campanha.getIdCampanha());
		ResultSet rs = stmt.executeQuery();
		rs.next();
		
		Campanha cp = new Campanha();
		cp.setIdCampanha(rs.getInt("id_campanha"));
		cp.setLocalDoacao(rs.getString("local_doacao"));
		cp.setEndereco(rs.getString("endereco"));
		cp.setTipoSanguineo(rs.getString("tipo_sanguineo"));
		cp.setNome(rs.getString("nome"));
		cp.setTelefone(rs.getString("telefone"));
		cp.setDataInicio(rs.getString("data_inicio"));
		cp.setDataFim(rs.getString("data_fim"));
		stmt.close();
		return cp;
	}
	
	public Campanha getCampanhaItem(Campanha campanha) throws Exception {
		Connection con = getConexao();
		String sql = "SELECT id_campanha, nome, tipo_sanguineo, local_doacao, endereco, telefone," + 
		"to_char(data_inicio, 'dd/mm/yyyy') as data_inicio, to_char(data_fim, 'dd/mm/yyyy') as data_fim " +
		"FROM campanha where id_campanha = ?";
		
		PreparedStatement stmt = con.prepareStatement(sql);
		stmt.setLong(1, campanha.getIdCampanha());
		ResultSet rs = stmt.executeQuery();
		rs.next();
		
		Campanha cp = new Campanha();
		cp.setIdCampanha(rs.getInt("id_campanha"));
		cp.setLocalDoacao(rs.getString("local_doacao"));
		cp.setEndereco(rs.getString("endereco"));
		cp.setTipoSanguineo(rs.getString("tipo_sanguineo"));
		cp.setNome(rs.getString("nome"));
		cp.setTelefone(rs.getString("telefone"));
		cp.setDataInicio(rs.getString("data_inicio"));
		cp.setDataFim(rs.getString("data_fim"));
		stmt.close();
		return cp;
	}
	
	public void remover(Campanha campanha) throws Exception {
		Connection con = getConexao();
		
		PreparedStatement stmt = con.prepareStatement("delete from Campanha where id_campanha = ?");
		stmt.setLong(1, campanha.getIdCampanha());
		stmt.execute();
		stmt.close();
		
	}
	
	public void atualizar(Campanha campanha) throws Exception {
	
		// Conex�o com o SGBD.
		Connection con = getConexao();
		
		// SQL
		String sql = "UPDATE campanha SET nome = ?, tipo_sanguineo = ?, local_doacao = ?, " +
				"endereco = ? , telefone = ?, data_inicio = to_date(?,'yyyy/mm/dd'), data_fim = to_date(?, 'yyyy/mm/dd') " +
				"WHERE id_campanha = ?";
		System.out.println(sql);
		// Preparando par�metros SQL
		PreparedStatement stmt = 
				con.prepareStatement(sql);
		
		stmt.setString(1, campanha.getNome());
		stmt.setString(2, campanha.getTipoSanguineo());
		stmt.setString(3, campanha.getLocalDoacao());
		stmt.setString(4, campanha.getEndereco());
		stmt.setString(5, campanha.getTelefone());
		stmt.setString(6, campanha.getDataInicio());
		stmt.setString(7, campanha.getDataFim());
		stmt.setLong(8, campanha.getIdCampanha());
		
		// Executando SQL
		stmt.executeUpdate();
		
		// Fechar conex�o com o SGBD.
		stmt.close();
		con.close();
		
	}
	
}
