package br.unp.sanguebom.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import br.unp.sanguebom.app.AppDAO;

public class SegurancaDAO extends AppDAO {

	
	/**
	 * M�todo utilizado para autenticar o usu�rio.
	 * 
	 * @param login
	 * @param senhaMD5
	 * @return
	 */
	public boolean autenticar(String login,
								String senhaMD5) {
		
		boolean autenticou = false;
		
		try {
			Connection con = getConexao();
			
			String sql = "SELECT u.id_usuario " +
							"FROM Usuario u " +
							"WHERE u.login = ? AND " +
							"u.senha = ?";
			
			PreparedStatement stmt = 
					con.prepareStatement(sql);
			
			stmt.setString(1, login);
			stmt.setString(2, senhaMD5);
			
			ResultSet rs = stmt.executeQuery();
			
			if(rs.next()) {
				autenticou = true;
			}
			
			stmt.close();
			con.close();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		
		
		return autenticou;
		
	}
	
	/**
	 * M�todo utilizado para autorizar o usu�rio.
	 * 
	 * @param login
	 * @param chave_recurso
	 * @return
	 */
	public boolean autorizar(String login,
							int chave_recurso) {
		
		boolean autorizado = false;
		
		try {
			Connection con = getConexao();
		
			String sql = "SELECT r.* " +
						"FROM Usuario u " +
							"INNER JOIN Usuario_Papel up " +
								"ON up.id_usuario = u.id_usuario " +
							"INNER JOIN Papel p " +
								"ON p.id_papel = up.id_papel " +
							"INNER JOIN Papel_Recurso pr " +
								"ON pr.id_papel = p.id_papel " +
							"INNER JOIN Recurso r " +
								"ON r.id_recurso = pr.id_recurso " +
						"WHERE u.login = ? " +
							"AND r.chave = ?";
			
			PreparedStatement stmt = 
					con.prepareStatement(sql);
			
			stmt.setString(1, login);
			stmt.setInt(2, chave_recurso);
			
			ResultSet rs = stmt.executeQuery();
			
			if(rs.next()) {
				autorizado = true;
			}
			
			stmt.close();
			con.close();
						
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return autorizado;
		
	}
	
	
}
