package br.unp.sanguebom.app;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *	Classe respons�vel pela conex�o com o SGBD.
 * 
 * @author Weinberg
 */
public class AppDAO {

	
	/*
	 * SQL Server:
	 * com.microsoft.sqlserver.jdbc.SQLServerDriver
	 * 
	 * PostgreSQL:
	 * org.postgresql.Driver
	 */
	private static String driverJDBC
					= "org.postgresql.Driver";
	/*
	 * SQL Server:
	 * jdbc:sqlserver://localhost:1433;
	 * 					databaseName=sanguebom;
	 * 
	 * PostgreSQL:
	 * jdbc:postgresql://localhost:5432/sanguebom
	 */
	private static String url
	= "jdbc:postgresql://localhost:5432/sanguebom";
	
	private static String login = "higor";
	private static String senha = "h1$10#ae";
	
	/**
	 * Conex�o com o SGBD.
	 * 
	 * @return
	 * @throws ClassNotFoundException
	 */
	public static Connection getConexao() 
			throws Exception {

		// Verificando driver
		Class.forName(driverJDBC);

		return DriverManager
				.getConnection(url, login, senha);
		
	}
	
	
}
