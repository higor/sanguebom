package br.unp.sanguebom.app;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



/**
 * Controlador padr�o que encapsula 
 * funcionalidades.
 * 
 * @author Weinberg
 */
abstract public class AppController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * Constante para definir o 
	 * diret�rio padr�o das JSPs.
	 */
	private static String baseJSP = "/WEB-INF/jsp/";
	
	
	/**
	 * Mensagens informativas do sistema.
	 */
	private ArrayList<String> mensagens 
		= new ArrayList<String>();
	
	
	public AppController(){
		
	}
	
	/**
	 * M�todo que encapsula a l�gica para
	 * redirecionamento.
	 * 
	 * @param view
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void forward(String view,
						HttpServletRequest request,
						HttpServletResponse response)
			throws ServletException, IOException {
		
		
		// Mensagens informativas
		if(mensagens != null && !mensagens.isEmpty()) {
			
			// Enviando para a JSP
			request.setAttribute("mensagens",
					mensagens);

			// Limpando mensagens
			mensagens = new ArrayList<String>();
		}
		
		// Redirecionando
		request
			.getRequestDispatcher(baseJSP + view)
			.forward(request, response);
	}


	public ArrayList<String> getMensagens() {
		return mensagens;
	}

	public void setMensagens(ArrayList<String> mensagens) {
		this.mensagens = mensagens;
	}
	
	public void addMensagem(String mensagem) {
		mensagens.add(mensagem);
	}
	
	public void getRecursos(HttpServletRequest request, HttpServletResponse response) {
		
	}
	

}
